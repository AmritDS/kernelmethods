import pickle
import numpy as np
import Utilities.kernelClassifier as kC
from Utilities.kernels import kernelLinear, kernelPoly, kernelRBF
from Utilities.LFIRE_Estimator import PosteriorEstimator
from Utilities.ExpModels import relativeError
from Utilities.ExpModels import  ArchModel, ARCH_TP

#Random Seed
randomSeed = 432

#------Exp Setup -------------------------------------------------#
# Model
model = ArchModel(-1,1, 0,1, randomSeed) 
#Number of samples to be created by each generator for classification.
n_samples=1000

#-------Load computed values from other scripts -------------------------------------------------------------------#
# 1
with open('observedData.pickle', 'rb') as f:
     (generatingParameters,obsData) = pickle.load(f)

#--------Run Evaluation -----------------------------------------------------------------------#
a = np.linspace(-0.9,0.9,10)
b = np.linspace(0.1,0.9,10)

x,y = np.meshgrid(a,b)
thetaVals = np.vstack([x.flatten(),y.flatten()]).T

with open('estimated_probabilitiesH1.pickle', 'rb') as f:
     estimated_probabilities  = pickle.load(f)

(probabilities,classifiers_test_scores) = estimated_probabilities
probabilities = np.swapaxes(probabilities,0,1)
p1 = classifiers_test_scores[:,0]

with open('estimated_probabilitiesH2.pickle', 'rb') as f:
     estimated_probabilities  = pickle.load(f)

(probabilities,classifiers_test_scores) = estimated_probabilities
probabilities = np.swapaxes(probabilities,0,1)
p1 = np.hstack([p1,classifiers_test_scores[:,0]])

with open('estimated_probabilitiesH3.pickle', 'rb') as f:
     estimated_probabilities  = pickle.load(f)

(probabilities,classifiers_test_scores) = estimated_probabilities
probabilities = np.swapaxes(probabilities,0,1)
p1 = np.hstack([p1,classifiers_test_scores[:,0]])

with open('estimated_probabilitiesH4.pickle', 'rb') as f:
     estimated_probabilities  = pickle.load(f)

(probabilities,classifiers_test_scores) = estimated_probabilities
probabilities = np.swapaxes(probabilities,0,1)
p1 = np.hstack([p1,classifiers_test_scores[:,0]])

with open('estimated_probabilitiesH5.pickle', 'rb') as f:
     estimated_probabilities  = pickle.load(f)

(probabilities,classifiers_test_scores) = estimated_probabilities
probabilities = np.swapaxes(probabilities,0,1)
p1 = np.hstack([p1,classifiers_test_scores[:,0]])

#tp =np.load('TruePosterior.npy')

#kD1 = model.klDivergence(tp,p1)

with open('scores.pickle', 'wb') as f:
     pickle.dump(p1,f)

