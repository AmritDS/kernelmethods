import numpy as np
from Utilities.ExpModels import HMM_stringGenerator

#Random Seed
randomSeed = 432

#------Exp Setup -------------------------------------------------#
# Model
model = HMM_stringGenerator(0,1, 0,1, randomSeed) 

#---Generate observed data ---------------------------------------#
#Number of datapoints to sample
n_datapoints = 100
(generatingParameres,obsData) = model.generate_ObsData(n_datapoints)
np.save('ObservedData',(generatingParameters,obsData))
