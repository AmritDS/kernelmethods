import numpy as np
import kernelClassifier as kC
from kernels import kernelLinear, kernelPoly, kernelRBF
from LFIRE_Estimator import PosteriorEstimator
from ExpModels import HMM_stringGenerator

#Random Seed
randomSeed = 432

#------Exp Setup -------------------------------------------------#
# Model
model = HMM_stringGenerator(0,1, 0,1, randomSeed) 
#Number of samples to be created by each generator for classification.
n_samples=1000


#-------Load computed values from other scripts -------------------------------------------------------------------#
# 1
[(beta,gamma,regScale1)] = np.load('parametersAndRegScales.npy')
kString =lambda X1,X2: stringKernel(X1,X2,beta,gamma) 
clf=kC.KernelClassifier(kString)

kernelsAndRegScales = [(clf,regScale)]

# 2
(generatingParameters,obsData) = np.save('ObservedData.npy')


#--------Run the ratio estimation algorithm -----------------------------------------------------------------------#
# values at which to compute the posterior
n1=10
n2=10
theta1_values=np.linspace( 0,1 , n1)
theta2_values=np.linspace( 0,1 , n2)
xv,yv = np.meshgrid(theta1_values,theta2_values)
theta_vals = np.dstack((xv,yv))
theta_values = theta_vals.reshape(n1*n2,2)

Estimator = PosteriorEstimator(randomSeed)
estimated_probabilities = Estimator.estimateProbabilities(observed_data, theta_values, model, n_samples, kernelsAndRegScales)

np.save('estimated_probabilities',(theta_values,estimated_probabilities))