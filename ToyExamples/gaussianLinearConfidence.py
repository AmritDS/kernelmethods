from __future__ import division
import numpy as np
import math
import matplotlib.mlab as mlab
import kernelClassifier as kC
#reload(kC) 
from sklearn import linear_model, model_selection
import matplotlib.pyplot as plt
import sys, os 
from scipy.stats import norm
#Random seed.
np.random.seed(431)

############################ Code generic to all data models: ########################################################

#Calculates the desired ratio for observed data, given mu value via ratio estimation.
def calc_ratio(X_class1, X_class2, obsData,kernel,figname,clf,regScale):
    y_vals_class1 = np.zeros((len(X_class1),1))+1
    y_vals_class2 = np.zeros((len(X_class2),1))-1

    y_vals = np.vstack([y_vals_class1,y_vals_class2])
    X_vals = np.vstack([X_class1,X_class2])

    datapoints=np.hstack([X_vals,y_vals])
    np.random.shuffle(datapoints)

    X_vals=datapoints[:,0:datapoints.shape[1]-1]
    y_vals=datapoints[:,datapoints.shape[1]-1]

    #Learn best weights for classification and get the log_ratio wrt observed data.
    log_ratio=classify(X_vals,y_vals,kernel,figname,clf,regScale).activation(obsData)

    #Calculate ratio.
    ratio=np.exp(np.clip(log_ratio,-500,500))
    
    return ratio


def classify(X_vals,y_vals,kernel,figname,clf,Cs):
    X_train,X_test,y_train,y_test = model_selection.train_test_split(X_vals,y_vals,test_size=0.30)
    
    bestScore = 0
    chosen_C= -1

    kf = model_selection.KFold(n_splits=5)
    #Choose a regularization parameter.
    for c in Cs:
        print("Training for c value: ",c)
        sum = 0
        for train, test in kf.split(X_vals):
            clf.train(X_vals[train],y_vals[train],c)
            sum += clf.score(X_vals[test],y_vals[test])
        average = sum/5
        plt.plot(np.log(c)/np.log(10),average,'b*')
        if(chosen_C==-1 or bestScore < average):
            chosen_C= c
            bestScore = average
        
    fig=plt.figure()        
    plt.plot(np.log(chosen_C)/np.log(10),bestScore,'r+')
    fig.savefig('./figures/'+figname+'.pdf')
    clf.train(X_vals,y_vals,chosen_C)
    print("Chosen c value: ",chosen_C)
    print("Classification accuracy: ",bestScore)
    return clf   


def estimateProbabilities(observed_data, prior, theta_values, generator_marginal, generator_thetaGiven, create_basis, n_samples, clf,regScale,kernel=kC.kernelLinear):
    #Observed data.
    observed_data_basis=create_basis(observed_data)
    
    #Sample from the marginal.
    marginal_samples=generator_marginal(n_samples)
    marginal_samples_basis=create_basis(marginal_samples)

    probabilities=[]
    for th in theta_values:
        print("-----------CALCULATING Theta VALUE : ",th," -------------------")
        #Sample from the given class.
        theta_samples=generator_thetaGiven(th,n_samples)
        theta_samples_basis=create_basis(theta_samples)
    
        prob= prior*calc_ratio(theta_samples_basis,marginal_samples_basis, observed_data_basis, kernel,str(th)+'_crossValidation',clf,regScale)
        probabilities.append(np.asscalar(prob))
    return np.array(probabilities)
##########################################################################################################################


#Takes a single value x and returns a b dimensional basis vector [1,x,x^2,...x^b]
def create_basis(msamp,b=3):
    basis=np.ones((len(msamp),1))
    for i in range (1,b):
        temp=np.power(msamp,i)
        basis=np.hstack([basis,temp])
    return basis

#Generates n 1-D samples from a gaussian ditribution with given mean.
def generator_thetaGiven(mu,n_samples):
    X = np.random.normal(mu,3,(n_samples,1))
    return X

#Generates n 1-D samples from gaussian ditributions, assuming a uniform dist. over means between [-5,5]
def generator_marginal(n_samples):
    mu= np.random.uniform(-20,20,(n_samples,1));
    X = np.random.normal(mu,3,(n_samples,1));
    return X

############################################# Algorithm1 ####################################################################
#Disable print statement -- The estimate Probabilities function prints a lot of info.
#sys.stdout = open(os.devnull, 'w')

#Observed data
observed_data = np.array([[0]])
#observed_data=np.random.normal(2.3,3,(1,1))

#Probability of mean.(Uniform prior -20,20)
prior=1/40

#Number of samples to be created by each generator for classification.
n_samples=1200

#Calculate posterior for mu_values 
mu_values=np.linspace(-2*3+ observed_data[0,0],2*3+ observed_data[0,0] ,10)

#Uses linear kernel by default -can specify custom kernel.
clf=kC.KernelClassifier(kC.kernelLinear)
regScale = np.array([1e-2,2*1e-1,5*1e-1,7*1e-1, 1 , 2 , 5 , 7 , 1e1, 2*1e1, 5*1e1, 7*1e1, 1e2,2*1e2,5*1e2,7*1e2,1e3])

expResults =estimateProbabilities(observed_data, prior, mu_values, generator_marginal, generator_thetaGiven, create_basis, n_samples,clf,regScale)
for exp in range(50): # Run the experiment 50 times

    #Uses linear kernel by default -can specify custom kernel.
    clf=kC.KernelClassifier(kC.kernelLinear)
    probabilities =estimateProbabilities(observed_data, prior, mu_values, generator_marginal, generator_thetaGiven, create_basis, n_samples,clf,regScale)

    expResults = np.vstack([expResults,probabilities])
    np.save('GaussiankernelLinearRUN2.npy',(mu_values,expResults))
#Enable print statement.
#sys.stdout = sys.__stdout__

