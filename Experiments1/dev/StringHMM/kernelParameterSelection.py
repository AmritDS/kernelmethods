import numpy as np
import Utilities.kernelClassifier as kC
import Utilities.hyperparameterChoice as hC
from Utilities.ExpModels import  HMM_stringGenerator
from Utilities.kernels import stringKernel

#Random Seed
randomSeed = 432

#------Exp Setup -------------------------------------------------#
# Model
model = HMM_stringGenerator(0,1, 0,1, randomSeed) 
typicalThetaValue = np.array([0.5,0.5])
#Number of samples to be created by each generator for classification.
n_samples=1000

#----Select parameters -------------------------------------------#
#Select parameter values and reg scale for the polynomial kernel.
kString =lambda X1,X2: stringKernel(X1,X2,4,0.5) #Any setting is fine.
clf=kC.KernelClassifier(kString)
(beta,gamma,regScale) =hC.autoBetaInt_GammaReal_and_RegScale(stringKernel,clf,model,typicalThetaValue, n_samples, numberOfOptions=20)

#----Save selections ---------------------------------------------#
parametersAndRegScales = [(beta,gamma,regScale)]

with open('parametersAndRegScales.pickle', 'wb') as f:
    pickle.dump(parametersAndRegScales, f)