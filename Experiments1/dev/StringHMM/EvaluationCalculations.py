import numpy as np
import kernelClassifier as kC
from kernels import kernelLinear, kernelPoly, kernelRBF
from LFIRE_Estimator import PosteriorEstimator
from ExpModels import relativeError

#Random Seed
randomSeed = 432

#-------Load computed values from other scripts -------------------------------------------------------------------#
# 1
(generatingParameters,obsData) = np.save('ObservedData.npy')
# 2
(theta_values,estimated_probabilities) = np.load('estimated_probabilities.npy')

#--------Run Evaluation -----------------------------------------------------------------------#
relError = relativeError(theta_values,estimated_probabilities,generatingParameters)
np.save('relativeError',relError)