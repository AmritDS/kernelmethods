import pickle
import numpy as np
import Utilities.kernelClassifier as kC
from Utilities.kernels import kernelLinear, kernelPoly, kernelRBF
from Utilities.LFIRE_Estimator import PosteriorEstimator
from Utilities.ExpModels import relativeError

#Random Seed
randomSeed = 432

#-------Load computed values from other scripts -------------------------------------------------------------------#
# 1
with open('observedData.pickle', 'rb') as f:
     observedData = pickle.load(f)
(generatingParameters,obsData) = observedData
# 2
with open('estimated_probabilities.pickle', 'rb') as f:
     (theta_values,estimated_probabilities) = pickle.load(f)

#--------Run Evaluation -----------------------------------------------------------------------#
relError = relativeError(theta_values,estimated_probabilities,generatingParameters.T)
np.save('relativeError',relError)